var nom = [];
var valeur = [];

function makeRequest(url) {

    var httpRequest = false;

    httpRequest = new XMLHttpRequest();

    if (!httpRequest) {
        alert('Abandon :( Impossible de créer une instance XMLHTTP');
        return false;
    }
    httpRequest.onreadystatechange = function() { alertContents(httpRequest); };
    httpRequest.open('GET', url, true);
    httpRequest.send(null);
}

function alertContents(httpRequest) {

    if (httpRequest.readyState == XMLHttpRequest.DONE) {

        //pour que les cercle dessiné au paravant dispparesent
        var ul = document.getElementById("content");
        while (ul.hasChildNodes()){
            ul.removeChild( ul.firstChild );  
        };

        //var riable pour selcte de mise en forme des cercle
        var config = liquidFillGaugeDefaultSettings();
        config.circleColor = "#ffffff";
        config.textColor = "#ffffff";
        config.waveTextColor = "#A6CFD5";
        config.waveColor = "#2799d1";
        config.waveAnimateTime = 3000;
        config.circleThickness = 0.02;

        if (httpRequest.status == 200) {
            
            var xmldoc = httpRequest.responseXML;
            //recuperer le nombre de cercle qu'il faut tracer
            var nombre = xmldoc.getElementsByTagName('nombrevaleurs').item(0).firstChild.data;
            nom = [];
            valeur = [];
            detail = [];
			
            for (var i = 0; i < nombre; i++) {
                //récuper les valeur contenu dans le document xml
                //les noms
                var root_node = xmldoc.getElementsByTagName('nom').item(i);
                nom.push(root_node.firstChild.data);
                //les valeures
                var root_node = xmldoc.getElementsByTagName('val').item(i);
                valeur.push(parseInt(root_node.firstChild.data));
                //les details
                var root_node = xmldoc.getElementsByTagName('detail').item(i);

                if(root_node.firstChild != null){
					detail.push(root_node.firstChild.data);
                }
                
                // cette li var contenir span , svg v p
                // rajouter une li dans le lu
                var li = document.createElement("li");
                ul.append(li);
                // ajouter une span dans le li pour pouvoir faire le requetes suivante
                var span = document.createElement("span");
                span.setAttribute("class", "affeau");

                if(detail[i] != null){
                	span.setAttribute("onclick", "makeRequest('"+detail[i]+"')");
                }
                
                li.append(span);
                //on rajoute une svg pour que le cercle puisse etre tracer
                var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
                span.append(svg);
                //ajout du p
                var p = document.createElement("p");
                span.append(p);
                p.innerHTML = nom[i];
                //on met un id au svg pour pouvoir appeler la fonction de tracer des cercles.
                svg.setAttribute("id","eau"+i);
                //tracer des cercles
            }
			
			for (var i = 0; i < nombre; i++) { 
				var g1 = loadLiquidFillGauge("eau"+i, valeur[i], config);
			}
			
        } else {
            alert('Un problème est survenu avec la requête.');
        }
    }
}